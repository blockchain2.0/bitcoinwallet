$(function () {
          $("#generate_transaction").click(function () {
            $.ajax({
              url: "/trans/generate",
              type: "POST",
              dataType : 'json',
              data: $('#transaction_form').serialize(),
              success: function(response){
                console.log(response);
                document.getElementById("confirmation_sender_address").value = response["transaction"]["sender_address"];
                document.getElementById("confirmation_recipient_address").value = response["transaction"]["recipient_address"];
                document.getElementById("confirmation_amount").value = response["transaction"]["value"];
                document.getElementById("transaction_signature").value = response["signature"];
                $("#basicModal").on('basicModal',function () {
                   $('#basicModal').trigger('focus');
                });
              },
              error: function(error){
                console.log(error);
              }
            });
          });
      });