$(document).ready(function(){
      $("#g_wallet").click(function() {
         $.ajax({
            url: "wallet/generate_wallet/new",
            type: 'GET',
            success: function (response) {
                document.getElementById("private_key").innerHTML = response['private_key'];
                document.getElementById("public_key").innerHTML = response['public_key'];
                document.getElementById("addr").innerHTML = response['address'];
                $("#passphare").html('passphare: '+response['passphare']);
                document.getElementById("warning").style.display = "block";
            },
            error: function(error){
              console.log(error);
            }
         });
      });
})
