$(document).ready(function () {
    $("#button_confirm_transaction").click(function () {
            //console.log($('#confirmation_transaction_form').serialize());
            $.ajax({
              url: "/trans/generate/new",
              type: "POST",
              dataType : 'json',
              data: JSON.stringify({"signature":$("#transaction_signature").val()}),
              success: function(response){
                console.log(response)
                //reset both forms
                $("#transaction_form")[0].reset();
                $("#confirmation_transaction_form")[0].reset();

                //clean text boxes
                $("#sender_address").val("");
                $("#sender_private_key").val("");
                $("#recipient_address").val("");
                $("#amount").val("");
                $("#success_transaction_modal").on('show.bs.modal', function () {
                    $("#success_transaction_modal").trigger({type:"click"})
                })

              },
              error: function(error){
                console.log(error);
              }
            });
          });
});