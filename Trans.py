from collections import OrderedDict

from cryptos import *

class Trans:
    def __init__(self,sender_address, sender_private_key, recipient_address,value):
        self.sender_address = sender_address
        self.sender_private_key = sender_private_key
        self.recipient_address = recipient_address
        self.value = value
    def __getattr__(self, attr):
        return self.data[attr]

    def to_dict(self):
        dict  = [{'value':self.value,'address':self.recipient_address}]
        return dict
    def dict(self,amount):
        return OrderedDict({'sender_address': self.sender_address,
                            'recipient_address': self.recipient_address,
                            'value': amount})

    def sign_transaction(self,private_key):
        """
        Sign transaction with private key
        """
        btc = Bitcoin(testnet=True)
        inputs = btc.unspent(self.sender_address)
        outs = self.to_dict()
        tx = btc.mktx(inputs,outs)
        sk = btc.signall(tx,private_key)
        return sk

    def push(self,sign):
        btc = Bitcoin(testnet=True)
        btc.pushtx(sign)
