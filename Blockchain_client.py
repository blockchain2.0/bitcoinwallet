from bitcoinlib.mnemonic import Mnemonic
from flask import Flask, render_template, jsonify, request
from bitcoin import sha256,pubtoaddr,privtopub
import  numpy as np
from cryptos import *
import os

from Trans import Trans

app = Flask(__name__)



@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')

@app.route('/wallet')
def wallet():
    return render_template('wallet.html')


@app.route('/wallet/generate_wallet/new')
def Generate_Wallet():
    passphare = Mnemonic().generate()
    private_key = sha256(passphare)
    public_key = privtopub(private_key)
    addr = pubtoaddr(public_key,111)
    response = {
        'private_key': private_key,
        'public_key': public_key,
        'address':addr,
        'passphare':passphare
    }
    return jsonify(response),200

@app.route('/trans')
def trans():
    return render_template('trans.html')


@app.route('/trans/generate', methods=['POST'])
def generate_transaction():
    response = {}
    if request.method == "POST":
        satoshi = 100000000
        sender_address = request.form["sender_address"]
        sender_private_key = request.form['sender_private_key']
        recipient_address = request.form['recipient_address']
        amount = float(request.form['amount'])
        value = np.long(amount*satoshi)
        tran = Trans(sender_address, sender_private_key, recipient_address, value)
        sk = tran.sign_transaction(tran.sender_private_key)

        response = {'transaction': tran.dict(amount),
                    'signature': sk}
    return jsonify(response),200

@app.route('/trans/generate/new',methods=['POST'])
def new_transaction():
    btc = Bitcoin(testnet=True)
    response = {}
    data =request.get_json(force=True)

    with open('trans.txt','w') as f:
        f.write(data["signature"])
    with open('trans.txt','r') as f:
        sk = f.read()
    print(sk)
    btc.pushtx(sk)
    os.remove('trans.txt')

    return jsonify(response), 200

if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('-p','--port',default=8080,type=int,help= 'port to listen on')
    args = parser.parse_args()
    port = args.port
    app.run(host= '172.16.0.200',port=port)
